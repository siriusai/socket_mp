class Socketio_server {
  load(io) {
    const socketmessage = new Socketio_server();
    let namaoption = null;
    let d = new Date();

    io.on('connection', (socket) => {
      console.log('a user connected');
      console.log(socket.id);
      socket.on('disconnect', () => {
        console.log('user disconnected', socket.id);
      });

      // join untuk masuk room ini wajib di buat untuk filter room
      socket.on('join', (room, optiondata) => {
        console.log(optiondata, room, 'join');
        socket.join(room, () => {
          // type  member 2 , admin 1
          socket.broadcast.to(room).emit('chatmessage', optiondata);
        });

        // socket.on('disconnect', () => {
        //   console.log('user disconnected', socket.id);
        //   socket.broadcast.to(optiondata.room).emit('chatmessage', datas2);
        // });
        // io.to(optiondata.room).emit('chatmessage', datas);
      });
      socket.on('leave', (room, optiondata) => {
        socket.leave(room, () => {
          // socket.broadcast.to(room).emit('chatmessage', '');
        });
      });
      socketmessage.message(socket, io, namaoption, d);
    });
  }
  message(socket, io, option, times) {
    socket.on('chatmessagebyroom', (room, optiondata) => {
      console.log(room, optiondata, 'test_server');
      socket.to(room).emit('chatmessage', optiondata);
      socket.emit('chatmessage', optiondata);
    });
  }
}
module.exports = Socketio_server;
