const express = require('express');
const app = express.Router();
let sendsocket = require('../socket/socketio');
let socket = new sendsocket();
app.use(express.static('./node_modules'));
app.get('/nama', socket.test);

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});
module.exports = app;
