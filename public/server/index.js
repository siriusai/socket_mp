let app = require('express')();
let http = require('http').createServer(app);
let consola = require('consola');
let socketio_server = require('../socket/socketio_server');
let socket_server_load = new socketio_server();
const router = require('./routers');
let io = require('socket.io')(http);
exports.io = io;
const host = process.env.HOST || '127.0.0.1';
const port = process.env.PORT || 3000;
app.use('/', router);
socket_server_load.load(io);

http.listen(3000);

consola.ready({
  message: `Server listening on http://${host}:${port}`,
  badge: true,
});
